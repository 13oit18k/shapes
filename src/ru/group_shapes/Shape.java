package ru.group_shapes;

/**
 * Родительский класс Shape
 *
 * @author Дунькин Вадим
 */
public abstract class Shape {


    abstract double Area();


}

