package ru.group_shapes;


/**
 * Класс квадрат
 *
 * @author Борисова Анастасия
 */
class Square extends Shape {
    public static double edge;


    public Square(double edge) {


        this.edge = edge;
    }


    @Override
    public double Area() {
        return Math.pow(edge, 2);
    }
}

