package ru.group_shapes;


/**
 * Класс с точками
 *
 * @author Дунькин Вадим
 */
public class Point {


    private double x;
    private double y;

    /**
     * Конструктор по умолчанию, устанавливает:
     * x = 0.0
     * y = 0.0
     */
    public Point() {
        this.x = 0.0;
        this.y = 0.0;
    }


    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Геттер абциссы
     *
     * @return абцисса
     */
    public double getX() {
        return x;
    }

    /**
     * Геттер ординаты
     *
     * @return ордината
     */
    public double getY() {
        return y;
    }

    /**
     * Вывод значений точки
     *
     * @return String
     */
    @Override
    public String toString() {
        return "{" + x + "," + y + '}';
    }
}