package ru.group_shapes;

/**
 * Класс треугольник
 *
 * @author Евстигнеев Максим.
 */

class Triangle extends Shape {
    public static Point p1;
    public static Point p2;
    public static Point p3;


    /**
     * Конструктор по умолчанию для создания треугольника
     */
    public Triangle() {


        this.p1 = new Point(5, 8);

        this.p2 = new Point(7, 2);

        this.p3 = new Point(6, 9);

    }


    /**
     * Подсчёт площади треугольника
     *
     * @return площадь
     */
    @Override
    public double Area() {
        return 0.5 * ((p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY()));
    }


}