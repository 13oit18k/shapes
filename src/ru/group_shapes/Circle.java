package ru.group_shapes;


/**
 * Класс круг
 *
 * @author Борисова Анастасия
 */
public class Circle extends Shape {

    public static double radius;
    final double pi = Math.PI;


    public Circle(double radius) {


        this.radius = radius;

    }


    @Override
    public double Area() {

        return pi * Math.pow(radius, 2);
    }


}