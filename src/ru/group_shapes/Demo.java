package ru.group_shapes;

/**
 * Демонстрационный класс
 *
 * @author Евстигнеев Максим,Хохлов Алексей
 */
public class Demo {

    public static void main(String[] args) {
        Shape[] shapes = {
                new Square(4),
                new Circle(6),
                new Triangle()
        };


        System.out.println("Радиус круга:" + Circle.radius);
        System.out.println("Площадь круга: " + Math.abs(shapes[1].Area()));
        System.out.println("Цвет круга: " + Color.Red +"\n");


        System.out.println("Ребро квадрата:" + Square.edge);
        System.out.println("Площадь квадрата: " + Math.abs(shapes[0].Area()));
        System.out.println("Цвет квадрата: " + Color.Black + "\n");


        System.out.println("Точка А: " + Triangle.p1);
        System.out.println("Точка B: " + Triangle.p2);
        System.out.println("Точка C: " + Triangle.p3);
        System.out.println("Площадь треугольника: " + Math.abs(shapes[2].Area()));
        System.out.println("Цвет треугольника: " + Color.Yellow + "\n");

        if (shapes[0].Area() > shapes[1].Area() && shapes[0].Area() > shapes[2].Area()) {
            System.out.println("Площадь квадрата больше");
        } else if (shapes[1].Area() > shapes[0].Area() && shapes[1].Area() > shapes[2].Area()) {
            System.out.println("Площадь круга больше");
        } else if (shapes[2].Area() > shapes[0].Area() && shapes[2].Area() > shapes[1].Area()) {
            System.out.println("Площадь треугольника больше ");
        } else {
            System.out.println("Фигуры равны.");

        }


    }
}